import axios from "axios";
import { api } from "../api.const";
import { authHeader } from "../_header";

class AuthService {
    login(login) {
        return axios.post(api.login, login);
    }

    getAccountInfo() {
      return axios.get(api.acount, {
        headers:authHeader()
      })
    }
}

export default new AuthService();
