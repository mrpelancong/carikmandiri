import axios from "axios";
import { api } from "../api.const";
import { authHeader } from "../_header";

class questionServices {
    get() {
      return axios.get(api.quisioner, {
        headers:authHeader()
      })
    }
}

export default new questionServices();
