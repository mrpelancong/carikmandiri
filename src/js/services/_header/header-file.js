export default function authHeaderFile() {
    const user = JSON.parse(localStorage.getItem('user'));

    if(user && user.id_token) {
        return {
          'Content-Type': 'multipart/form-data',
            Authorization: 'Bearer ' + user.id_token,

        };
    } else {
        return {};
    }
}
