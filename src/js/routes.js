
import Login from '../pages/login.jsx';
import { f7 } from 'framework7-react';

import HomePage from '../pages/home.jsx';
import AboutPage from '../pages/about.jsx';
import FormPage from '../pages/form.jsx';

import DynamicRoutePage from '../pages/dynamic-route.jsx';
import RequestAndLoad from '../pages/request-and-load.jsx';
import NotFoundPage from '../pages/404.jsx';

  // tabs
  import tabHome from '../pages/mandiri/home.jsx'
  import tabProfile from '../pages/mandiri/profile.jsx'
  import tabTentang from '../pages/mandiri/tentang.jsx'

  // home 
  import BangunanPage from '../pages/mandiri/bangunan'
  import KeluargaPage from '../pages/mandiri/keluarga'

var routes = [
  {
    path: '/',
    async({ resolve }) {
      console.log(f7.store.state);
      
      const reactComponent = () => import('../pages/login.jsx');
      reactComponent().then((rc) => {
        // resolve with component
        resolve({ component: rc.default })
      });
    }
  },
  {
    path: '/home/',
    component: HomePage,
    tabs: [
      {
        path: '/',
        id: 'tab1',
        component: tabHome,
      },
      {
        path: '/tab2',
        id: 'tab2',
        component: tabProfile,
      },
      {
        path: '/tab3',
        id: 'tab3',
        component: tabTentang,
      },
    ],
  },
  {
    path: '/about/',
    component: AboutPage,
  },
  {
    path: '/bangunan/',
    component: BangunanPage,
  },
  {
    path: '/keluarga/',
    component: KeluargaPage,
  },
  {
    path: '/form/',
    component: FormPage,
  },
  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    component: DynamicRoutePage,
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function ({ router, to, resolve }) {
      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = to.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            props: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  {
    path: '(.*)',
    component: NotFoundPage,
  },
];

export default routes;
