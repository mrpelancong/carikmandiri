import axios from 'axios'
import { AuthService } from './services'
import { createStore } from 'framework7/lite';
import { f7 } from 'framework7-react';
const store = createStore({
  state: {
    error : false, errMsg : null,
    loginStatus : false, group : null,
    token : null,
    userDetail: null,
    quisioner : {
      data : null,
      error : false,
      errorMsg : null,
    }
  },
  getters: {
    token({ state }) {
      return state.token;
    },
    userDetail({ state }) {
      return state.userDetail;
    },
    quisioner({ state }) {
      return state.quisioner;
    }, 
    errStatus({ state }){
      return state.error;
    },
    errMsg({ state }){
      return state.errMsg;
    },
    loginStatus({ state }){
      return state.loginStatus;
    },
    token({ state }){
      return state.token;
    },
  },
  actions: {
    errorAct({ state }, error){
      state.error = error.status;
      state.errMsg = error.errMsg;
    },
    loginAct({ state, dispatch }, login) {
      f7.preloader.show();
      try {
        AuthService.login(login).then(res => {
            state.token = res.data.id_token;
            state.userDetail = res.data.user;
            state.token = res.data.id_token;
            state.group = res.data.group;
            state.loginStatus = true;
        }).catch(err => {
          dispatch('errorAct', {
            status : true,
            errMsg : 'Terjadi Kesalahan...'
          });
          f7.dialog.preloader('Terjadi Kesalahan...');
        }).then(() => {
          f7.preloader.hide();
          setTimeout(() => {
            f7.dialog.close();
          }, 3000);
        })
      } catch (error) {
        dispatch('errorAct', {
          status : true,
          errMsg : 'Terjadi Kesalahan...'
        })
      }
    },
    loadQuisinoner({ state }, data) {
      axios.get('/static/json/question.json').then(res => {
        console.log('quisioner ', res);
      })
    }
  },
})
export default store;
