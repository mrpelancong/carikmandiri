import React, { useState, useEffect } from 'react';

import {
  f7ready,
  App,
  View, 
} from 'framework7-react';
import { getDevice } from 'framework7';

import routes from '../js/routes';
import store from '../js/store';

const MyApp = () => {
  const device = getDevice();
  const f7params = {
    name: 'Carik Mandiri', // App name
      theme: 'auto', // Automatic theme detection
      store: store,
      routes: routes,
  };

  f7ready(() => {
    if(device.os === 'windows') {
      // window.open('http://carik.jakarta.go.id/', "_self")
    }
  });

  return (
    <App { ...f7params } >
        {/* Your main view, should have "view-main" class */}
        <View main className="safe-areas" url="/" browserHistory={true} pushStateSeparator="" />
    </App>
  )
}
export default MyApp;