import React, { useState, useEffect } from 'react';
import {
  f7,
  Page,
  LoginScreenTitle,
  List,
  ListInput,
  Button,
  BlockFooter, useStore
} from 'framework7-react';

export default ({ f7router }) => {
  const [username, setUsername] = useState('p01952');
  const [password, setPassword] = useState('Carik19');
  const loginStatus = useStore('loginStatus');

  const signIn = () => {
    f7.store.dispatch('loginAct', {
      'username' : username,
      'password' : password
    })
  };

  if (loginStatus) {
    f7router.navigate('/home/');
  }

  return (
    <Page noToolbar noNavbar noSwipeback loginScreen>
        <LoginScreenTitle>
          <img src="/static/icons/favicon.png" width="80" alt=""/> <br/>
          Carik Mandiri
        </LoginScreenTitle>
        <List form>
            <ListInput
            label="Username"
            type="text"
            placeholder="Nomor Induk Keluarga"
            value={username}
            onInput={(e) => {
                setUsername(e.target.value);
            }}
            />
            <ListInput
            label="Password"
            type="password"
            placeholder="Password"
            value={password}
            onInput={(e) => {
                setPassword(e.target.value);
            }}
            />
        </List>
        <List>
            <BlockFooter>
              <Button fill onClick={signIn}>
                Masuk
              </Button>
            </BlockFooter>
        </List>
    </Page>
  );
};