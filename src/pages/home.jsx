import React, { useState, useRef } from 'react';
import {
  Page,
  Navbar,
  Sheet,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Block,
  BlockTitle,
  List,
  ListItem,
  Row,
  Col,
  Button
} from 'framework7-react';
import { Tabs, Tab, Fab, Icon, FabButtons, FabButton } from 'framework7-react';

const HomePage = () => {
  const [sheetOpened, setSheetOpened] = useState(false);
  const sheet = useRef(null);

  return (
    <Page name="home">
      {/* Top Navbar */}
      <Navbar sliding={false}>
        <NavTitle sliding>Carik Mandiri</NavTitle>
        <NavRight>
          <Link iconIos="f7:bell_circle_fill" iconAurora="f7:bell_circle_fill" iconMd="material:notifications_active"/>
        </NavRight>
      </Navbar>
      {/* Toolbar */}
      <Fab position="right-bottom" slot="fixed" color="white" onClick={() => {
              setSheetOpened(true);
            }}>
        <img src="/static/icons/whatsapp.png" width="32" alt=""/>
      </Fab>
      <Sheet
        opened={sheetOpened}
        onSheetClosed={() => {
          setSheetOpened(false);
        }}
        style={{ height: 'auto', '--f7-sheet-bg-color': '#fff' }}
        swipeToClose
        swipeToStep
        backdrop
      >
        <div className="sheet-modal-swipe-step">
          <div className="display-flex padding justify-content-space-between align-items-center">
            <div style={{ fontSize: '18px' }}>
              <b>Hubungi Kader Dasawisma</b>
            </div>
            <div style={{ fontSize: '22px' }}>
            <Icon ios="f7:phone" aurora="f7:phone" md="material:call"></Icon>
            </div>
          </div>
          <div className="padding-horizontal padding-bottom">
            <Button large fill>
              Kirim Pesan Whatsapp
            </Button>
            <div className="margin-top text-align-center">Swipe Up Untuk Detail Kader</div>
          </div>
        </div>
        <List noHairlines>
          <ListItem title="Nama Lengkap">
            <b slot="after" className="text-color-black">
              Siti Khumairah 
            </b>
          </ListItem>
          <ListItem title="Kelompok Dasawisma">
            <b slot="after" className="text-color-black">
              Mawar 001
            </b>
          </ListItem>
        </List>
      </Sheet>
      {/*  */}
      <Toolbar bottom tabbar>
        <Link 
          tabLink 
          href="./" 
          routeTabId="tab1"
          text="Home"
          iconIos="f7:envelope_fill"
          iconAurora="f7:envelope_fill"
          iconMd="material:home"
          />
        <Link 
          tabLink 
          href="./tab2" 
          routeTabId="tab2"
          text="Akun"
          iconIos="f7:envelope_fill"
          iconAurora="f7:envelope_fill"
          iconMd="material:account_circle"
          />
        <Link 
          tabLink 
          href="./tab3" 
          routeTabId="tab3"
          >
            <img src="/static/icons/favicon.png" width="32" alt=""/>
          </Link>
      </Toolbar>
      <Tabs routable style={{
        backgroundColor: 'aliceblue',
        height:'100%'
      }}>
        <Tab className="page-content" id="tab1" />
        <Tab className="page-content" id="tab2" />
        <Tab className="page-content" id="tab3" />
      </Tabs>
    </Page>
  )
};
export default HomePage;