import React from 'react';
import { Page, Navbar, Block, BlockTitle, Fab, Icon, FabButtons,FabButton  } from 'framework7-react';

const Keluarga = () => (
  <Page>
    <Navbar title="Informasi Keluarga" backLink="Back" />
    <Fab position="right-bottom" slot="fixed" text="Pilih Pertanyaan" color="red">
      <Icon ios="f7:question_circle_fill" aurora="f7:question_circle_fill" md="material:help"></Icon>
      <FabButtons position="top">
        <FabButton label="Informasi Dasar">
            <Icon ios="f7:person_3_fill" aurora="f7:person_3_fill" md="material:people"></Icon>
        </FabButton>
        <FabButton label="Kontrasepsi">
            <Icon ios="f7:home" aurora="f7:home" md="material:security"></Icon>
        </FabButton>
        <FabButton label="Pembangunan Keluarga">
            <Icon ios="f7:tree" aurora="f7:tree" md="material:home"></Icon>
        </FabButton>
        <FabButton label="Aset Keluarga">
            <Icon ios="f7:local_fire_department" aurora="f7:local_fire_department" md="material:monetization_on"></Icon>
        </FabButton>
        <FabButton label="Ketahanan Keluarga">
            <Icon ios="f7:local_fire_department" aurora="f7:local_fire_department" md="material:verified_user"></Icon>
        </FabButton>
      </FabButtons>
    </Fab>
  </Page>
);

export default Keluarga;
