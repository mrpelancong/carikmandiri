import React, { useState } from 'react';
import { List, ListItem, ListInput, Icon, Card, CardHeader, CardContent } from 'framework7-react';

export default ({ f7router }) => {
    const [selected, setSelected] = useState('home');
   const [selectedMedia, setSelectedMedia] = useState('home');
   
  return (
    <div className="block display-flex flex-direction-column justify-content-center" >
        <div className="align-self-center">
            <img src="/static/icons/user.png" width="100" alt=""/>
        </div>
        <List mediaList inset>
            <ListItem 
                link="#" 
                title="Friski Kasviko" 
                subtitle="1603032605970001"
                className="no-chevron"
            >
            </ListItem>
        </List>
        <List noHairlinesMd  className="shadow-sm">
            <ListInput
                label="Username"
                type="text"
                placeholder="Username"
                clearButton
            />

            <ListInput
                label="Nomor Telepon"
                type="text"
                placeholder="Nomor Telepon"
                clearButton
            />

            <ListInput
                label="Email"
                type="text"
                placeholder="Email"
                clearButton
            />
        </List>
        <List className="linkList" noHairlinesMd>
            <ListItem
            link
            title="Ganti Password"
            >
            <Icon md="material:phonelink_lock" aurora="f7:house_fill" ios="f7:house_fill" slot="media" />
            </ListItem>
        </List>
        <List className="linkList" noHairlinesMd>
            <ListItem
                title="Keluar Aplikasi"
                link
                onClick={() => f7router.navigate('/') }
            >
            <Icon md="material:power_settings_new" aurora="f7:house_fill" ios="f7:house_fill" slot="media" />
            </ListItem>
        </List>
    </div>
  );
};