import React, { useState } from 'react';
import { List, ListItem, Card, CardHeader, CardContent, Link } from 'framework7-react';

export default ({ f7router }) => {

  return (
    <div className="block">
        <List mediaList inset className="shadow-sm">
            <ListItem 
                link="#" 
                title="Friski Kasviko" 
                subtitle="1603032605970001"
                className="no-chevron"
            >
                <img
                    slot="media"
                    src="/static/icons/favicon.png"
                    width="44"
                />
            </ListItem>
        </List>
        {/* Card */}
        <Link
                href="/bangunan/"
                style={{
                    width: '100%',
                    display: 'block'
                }}
                animate={false}
            >
        <Card className="demo-card-header-pic">
                <CardHeader
                    className="no-border"
                    valign="bottom"
                    style={{
                        backgroundImage: 'url(/static/img/home.jpg)',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                        height:'150px'
                    }}
                >
                </CardHeader>
                <CardContent>
                    <strong> Data Bangunan </strong>
                </CardContent>
        </Card>
        </Link>
        <Link
                href="/keluarga/"
                style={{
                    width: '100%',
                    display: 'block'
                }}
                animate={false}
            >
            <Card className="demo-card-header-pic">
                <CardHeader
                    className="no-border"
                    valign="bottom"
                    style={{
                        backgroundImage: 'url(/static/img/keluarga.jpg)',
                        backgroundSize: 'cover',
                        backgroundPosition: 'center',
                        height:'150px'
                    }}
                >
                </CardHeader>
                <CardContent>
                    <strong> Data Keluarga </strong>
                </CardContent>
            </Card>
        </Link>
    </div>
  );
};