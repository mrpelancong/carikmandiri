import React, { useState } from 'react';
import { List, ListItem, Card, CardHeader, CardContent, Block } from 'framework7-react';

export default ({ f7router }) => {

  return (
    <div className="block display-flex flex-direction-column justify-content-center" style={{
        height: '100%'
    }}>
        <div className="align-self-center">
            <img src="/static/icons/favicon.png" width="100" alt=""/>
        </div>
        <div className="text-align-center">
            <h1>Carik Jakarta</h1> 
            <i>Web Apps Version : 1.0.0</i> 
            <p>Aplikasi Pendataan Keluarga 2021 Mandiri berbasis Web Apps.</p>
        </div>
    </div>
  );
};