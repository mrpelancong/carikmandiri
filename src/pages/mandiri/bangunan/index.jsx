import React from 'react';
import { Page, Navbar, Block, BlockTitle, Fab, Icon, FabButtons,FabButton  } from 'framework7-react';

const Bangunan = () => (
  <Page>
    <Navbar title="Informasi Bangunan" backLink="Back" />
    <Fab position="right-bottom" slot="fixed" text="Pilih Pertanyaan" color="red">
      <Icon ios="f7:question_circle_fill" aurora="f7:question_circle_fill" md="material:help"></Icon>
      <FabButtons position="top">
        <FabButton label="Informasi Dasar">
            <Icon ios="f7:home" aurora="f7:home" md="material:home"></Icon>
        </FabButton>
        <FabButton label="Kriteria Rumah Sehat">
            <Icon ios="f7:home" aurora="f7:home" md="material:home"></Icon>
        </FabButton>
        <FabButton label="Pemanfataan Lahan">
            <Icon ios="f7:tree" aurora="f7:tree" md="material:terrain"></Icon>
        </FabButton>
        <FabButton label="Penanggulangan Kebakaran">
            <Icon ios="f7:local_fire_department" aurora="f7:local_fire_department" md="material:whatshot"></Icon>
        </FabButton>
      </FabButtons>
    </Fab>
  </Page>
);

export default Bangunan;
